package main

import (
	"flag"
	"path/filepath"
)

var configInput string
var config Config

func init() {
	flag.StringVar(&configInput, "config", "", "json file that acts as a configuration")
}

func main() {
	flag.Parse()
	if configInput == "" {
		panic("config file is required")
	}
	filepath.Clean(configInput)
	config.getConfigFromFile(configInput)
}
