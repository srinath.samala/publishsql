package main

import (
	"encoding/json"
	"io/ioutil"
)

//Connection maps to each connection object in config json file
type Connection struct {
	ConnectionString string `json:"connection_string"`
	Skip             bool   `json:"skip"`
}

//Config  maps to config json file
type Config struct {
	Connections            []Connection `json:"connections"`
	AsyncConnections       bool         `json:"async_connections"`
	IndependentScriptFiles bool         `json:"async_scripts"`
	ResultStorageFolder    bool         `json:"batch_results"`
	BatchScriptFolder      bool         `json:"batch_folder_path"`
	Driver                 string       `json:"driver"`
}

//getConfig serialize the object
func (config *Config) getConfig(input []byte) {
	json.Unmarshal(input, config)
}

//getConfigFromFile fills config file
func (config *Config) getConfigFromFile(fileName string) {
	configArray, err := ioutil.ReadFile(fileName)
	if err != nil {
		panic(err)
	} else {
		config.getConfig(configArray)
	}
}
